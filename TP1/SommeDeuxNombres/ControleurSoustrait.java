import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurSoustrait implements EventHandler<ActionEvent>{
    private AppliSomme appli;
    
    public ControleurSoustrait(AppliSomme appli){
        this.appli = appli;
    }
    
    public void handle(ActionEvent e){
        System.out.println("Soustrait");
        this.appli.soustrait();
    }
}