import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurMultiplie implements EventHandler<ActionEvent>{
    private AppliSomme appli;
    
    public ControleurMultiplie(AppliSomme appli){
        this.appli = appli;
    }
    
    public void handle(ActionEvent e){
        System.out.println("Multiple");
        this.appli.soustrait();
    }
}