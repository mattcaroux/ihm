import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.event.EventHandler;

public class ControleurClavier implements EventHandler<KeyEvent>{
    private AppliDessin appli;
    
    public ControleurClavier(AppliDessin appli){
        this.appli = appli;
    }
    
    public void handle(KeyEvent e){       
        //System.out.println(e.getCode());
        if (e.getCode().equals(KeyCode.ADD)){ // est ce qu'il a appuyer sur une touche particuliere
            System.out.println("+");
            this.appli.augmenteLeDernierCercle();
        }
        if (e.getCode().equals(KeyCode.MINUS)){ // est ce qu'il a appuyer sur une touche particuliere
            System.out.println("-");
            this.appli.diminueLeDernierCercle();
        }
        if (e.getCode().equals(KeyCode.SUBTRACT)){ // est ce qu'il a appuyer sur une touche particuliere
            System.out.println("-");
            this.appli.diminueLeDernierCercle();
        }
        if (e.getCode().equals(KeyCode.MULTIPLY)){ // est ce qu'il a appuyer sur une touche particuliere
            System.out.println("*");
            this.appli.changeCouleurDuDernierDisque();
        }
}
}