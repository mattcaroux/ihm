import org.w3c.dom.events.Event;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControlleurBoutonPlus implements EventHandler<ActionEvent>{
    private Calculette calculette;
    private AppliCalculatrice appli;

    public ControlleurBoutonPlus(AppliCalculatrice appli, Calculette calculette){
        this.appli = appli;
        this.calculette = calculette;

    }


    @Override
    public void handle(ActionEvent event){
        double val1;
        double val2;
        try{
            System.out.println("+");
            val1 = this.appli.getTF1();
            this.calculette.setNombre1(val1);
            val2 = this.appli.getTF2();
            this.calculette.setNombre2(val2);
            this.calculette.additionner();
            this.appli.majResultat();
        }
        catch (NumberFormatException exp){
            System.out.println("Ce n'est pas un nombre");
        }
    }
}

