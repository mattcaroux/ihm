public class Calculette{
    private double nombre1;
    private double nombre2;
    private double resultat;

    public double additionner(){
        resultat = this.nombre1+this.nombre2;
        return resultat;
    }
    public double soustraire(){
        resultat = this.nombre1-this.nombre2;
        return resultat;
    }
    public double multiplie(){
        resultat = this.nombre1*this.nombre2;
        return resultat;
    }
    public double divise(){
        resultat = this.nombre1/this.nombre2;
        return resultat;
    }
    public void setNombre1(double n){
        this.nombre1 = n;
    }
    public void setNombre2(double n){
        this.nombre2 = n;
    }
    public double getResultat(){
        return this.resultat;
    }
}