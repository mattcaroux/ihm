import javafx.application.Application;
import javafx.application.Platform;
//import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;

public class AppliCalculatrice extends Application{
    private TextField textFieldNb1;
    private TextField textFieldNb2;
    private Label resultat;
    private Calculette calculette;


    @Override
    public void init(){
        this.calculette = new Calculette();
        this.textFieldNb2 = new TextField();
        this.textFieldNb1 = new TextField();
        this.resultat = new Label();
    }


    @Override
public void start(Stage stage) throws Exception{
    BackgroundFill backgroundFill = new BackgroundFill(Color.BLUE, null, null);
    Background background = new Background(backgroundFill);
    
    Scene scene = new Scene(root());
    scene.setFill(Color.BLUE); // Pour changer la couleur de la bordure de la fenêtre
    stage.setTitle("Calculatrice");
    stage.setScene(scene);
    stage.show();
}


    private HBox hbox() {
        HBox pane = new HBox(50);
        Button boutonAddition = new Button("+");
        Button boutonMultiplication = new Button("*");
	    Button boutonDivision = new Button("÷");
        Button boutonSoustraire = new Button("-");

        boutonAddition.setStyle("-fx-background-color: #dc143c;");
        boutonSoustraire.setStyle("-fx-background-color: #afeeee;");
        boutonMultiplication.setStyle("-fx-background-color: #9932cc;");
        boutonDivision.setStyle("-fx-background-color: #20b2aa;");


        boutonAddition.setTextFill(Color.WHITE);
        boutonSoustraire.setTextFill(Color.WHITE);
        boutonMultiplication.setTextFill(Color.WHITE);
        boutonDivision.setTextFill(Color.WHITE);



        boutonAddition.setOnAction(new ControlleurBoutonPlus(this, calculette));
        boutonSoustraire.setOnAction(new ControlleurBoutonMoins(this, calculette));
        boutonMultiplication.setOnAction(new ControlleurBoutonMultiplie(this, calculette));
        boutonDivision.setOnAction(new ControlleurBoutonDivise(this, calculette));
        pane.getChildren().addAll(boutonAddition, boutonSoustraire, boutonDivision, boutonMultiplication);
        pane.setAlignment(Pos.TOP_CENTER);
        return pane;
    }

    private HBox root(){
        HBox pane = new HBox(10);
        VBox vbox = new VBox(50);
        vbox.getChildren().addAll(gridPane(), hbox());
        vbox.setPrefWidth(700);
        HBox.setMargin(vbox, new Insets(50));
        pane.getChildren().add(vbox);
        return pane;
    }
    public double getTF1(){
        return Double.parseDouble(textFieldNb1.getText());
    }
    public double getTF2(){
        return Double.parseDouble(textFieldNb2.getText());
    }
    public void majResultat(){
        this.resultat.setText(calculette.getResultat()+"");
    }
    private GridPane gridPane() {
        GridPane pane = new GridPane();
        pane.add(new Label("Nombre 1"), 0, 0);
        pane.add(new Label("Nombre 2"), 2, 0);
        pane.add(new Label("Résultat : "), 0, 1);
        pane.add(textFieldNb1, 1, 0);
        pane.add(textFieldNb2, 3, 0);
        pane.add(resultat, 1, 1, 3, 1);
        pane.setHgap(20);
        pane.setVgap(30);
        return pane;
    }

    private void ajouteTitre(Pane root){
        HBox hbTitre = new HBox(20);
        Label titre = new Label("Une calculatrice");
        hbTitre.setAlignment(Pos.CENTER);
        root.getChildren().add(hbTitre);
    }
    public static void main(String[] args){
        launch(args);
    }
}