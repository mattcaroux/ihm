

javac -d bin src/*.java --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls

java -cp bin:img --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls AppliConverter

javadoc -d doc src/*.java --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls