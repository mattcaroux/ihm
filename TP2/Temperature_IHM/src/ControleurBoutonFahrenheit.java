import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonFahrenheit implements EventHandler<ActionEvent>{

    private Temperature temperature;
    private AppliConverter appli;
    
    public ControleurBoutonFahrenheit(Temperature temperature, AppliConverter appli){
        this.appli = appli;
        this.temperature = temperature;
    }

    @Override
    public void handle(ActionEvent event) {
        double value;
        value = this.appli.getValueFahrenheit();
        this.temperature.setvaleurFahrenheit(value);     
        this.appli.majTF();
    }
}