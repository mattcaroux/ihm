import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonCelsius implements EventHandler<ActionEvent>{

    private Temperature temperature;
    private AppliConverter appli;
    
    public ControleurBoutonCelsius(Temperature temperature, AppliConverter appli){
        this.appli = appli;
        this.temperature = temperature;
    }

    @Override
    public void handle(ActionEvent event) {
        try{double value;
        value = this.appli.getValueCelcius();
        this.temperature.setvaleurCelcius(value);     
        this.appli.majTF();
        }
        catch(NumberFormatException exp){
            this.appli.effaceTF();
        }
    }
}