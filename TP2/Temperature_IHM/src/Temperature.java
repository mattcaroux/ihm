public class Temperature {
    
    private double valeurCelcius;
    
    /**
     * Par défaut, la température est initialise à 0°C
     */
    public Temperature(){
        this.valeurCelcius = 0;
    }

    /**
     * @param  temperature  une tempéraure en degré Celcius (°C)
     */
    public Temperature(double temperature){
        this.valeurCelcius = temperature;
    }
    /**Permet de retourner la valeur de la température en Celsius
     * 
     * @return (type) double
     */
    public double valeurCelcius(){
        return this.valeurCelcius;
    }
    /**Permet de retourner la valeur de la température en Fahrenheit
     * 
     * @return (type) double
     */
    public double valeurFahrenheit(){
        return this.valeurCelcius * 1.8 + 32;
    }
    /**Permet de retourner la valeur de la température en Kelvin
     * 
     * @return (type) double
     */
    public double valeurKelvin(){
        return this.valeurCelcius + 273.15;
    }

    /**
     * @param  nouvelleValeurCelcius  une tempéraure en degré Celcius (°C)
     */
    public void setvaleurCelcius(double nouvelleValeurCelcius){
        this.valeurCelcius = nouvelleValeurCelcius;
    }

    /**
     * @param  nouvelleValeurFahrenheit  une tempéraure en degré Fahrenheit (°F)
     */
     public void setvaleurFahrenheit(double nouvelleValeurFahrenheit){
        double nouvelleValeurCelcius = (nouvelleValeurFahrenheit - 32)/1.8;
        this.valeurCelcius = nouvelleValeurCelcius;
    }
    /**Permet de generer une nouvelle température a partir de la valeur en Celsius
     * 
     * @param nouvelleValeurKelvin une temperature en degré Kelvin (°K)
     */
    public void setvaleurKelvin(double nouvelleValeurKelvin){
        double nouvelleValeurCelcius = nouvelleValeurKelvin - 273.15;
        this.valeurCelcius = nouvelleValeurCelcius;
    }

    @Override
    public String toString(){
        return this.valeurCelcius+"°C = "+ this.valeurFahrenheit()+"°F"+ this.valeurKelvin()+"°K";
    }
}

