import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonKelvin implements EventHandler<ActionEvent>{

    private Temperature temperature;
    private AppliConverter appli;
    
    public ControleurBoutonKelvin(Temperature temperature, AppliConverter appli){
        this.appli = appli;
        this.temperature = temperature;
    }

    @Override
    public void handle(ActionEvent event) {
        double value;
        value = this.appli.getValueKelvin();
        this.temperature.setvaleurKelvin(value);     
        this.appli.majTF();
    }
}