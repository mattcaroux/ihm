import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.*;

public class AppliConnexion extends Application{

    protected Label Titre;
    protected TextField Identifiant ;
    protected TextField MotDePasse ;


    @Override
    public void start(Stage primaryStage) throws Exception {
        // Construction du graphe de scène
        GridPane root = new GridPane();
        root.setVgap(10);
        root.setHgap(10);
        //root.setPadding(null);

        this.ajouteTitre(root);
        this.ajouteIdentifiant(root);
        this.ajouteMotDePasse(root);
        this.ajouteBoutons(root);
        
        Scene scene = new Scene(root);
        primaryStage.setTitle("Allo 45");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private void ajouteBoutons(GridPane root) {
        Button btnConnexion = new Button("Connexion");
        root.add(btnConnexion, 1, 3);
    }


    private void ajouteMotDePasse(GridPane root) {
        Label labelMdp = new Label("Mot de passe : ");
        root.add(labelMdp, 0, 2);
        TextField motDePasse = new TextField("");
        root.add(motDePasse, 1, 2);
    }


    private void ajouteIdentifiant(GridPane root) {
        Label labelId = new Label("Identifiant : ");
        root.add(labelId, 0, 1);
        TextField identifiant = new TextField("");
        root.add(identifiant, 1, 1);

    }


    private void ajouteTitre(GridPane root) {
        HBox hbTitre = new HBox(20);
        Label titre = new Label("Entrez votre identifiants et votre mot de passe");
        hbTitre.getChildren().addAll(titre);
        root.add(hbTitre, 0, 0, 3, 1);
    }
    public static void main(String[] args) {
        launch(args);
    }
}