import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.border.TitledBorder;

import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.*;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.*;

public class AppliHarry extends Application{

    protected TextField pseudo;
    protected PasswordField MotDePasse;
    protected ComboBox poste;
    private Scene scene;
    private Button debut;
    private Button prec;
    private Button valider;
    private Button nouveau;
    private Button suiv;
    private Button dern;


    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        //root.setVgap(10);
        //root.setHgap(10);

        this.ajouteTitre(root);
        this.ajouteGauche(root);
        this.ajouteCentre(root);
        this.ajouteBas(root);

        
        this.scene = new Scene(root);
        primaryStage.setTitle("Gestion d'un club de Quidditch");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void ajouteTitre(BorderPane root){
        BorderPane border = new BorderPane();//Je met le titre au centre haut de la fenetre
        Label titre = new Label("Consultation/modifiction d'une fiche joueur");
        titre.setFont(Font.font("Arial", FontWeight.THIN, 32));
        titre.setAlignment(Pos.CENTER);
        border.setCenter(titre);
        root.setTop(border);
    }
    public void ajouteGauche(BorderPane root){
        VBox box = new VBox(); //Je crée des boutons
        box.setStyle("-fx-background-color: #FFC0CB;");
        box.setSpacing(5);



        Button debut = new Button("Début");
        Button prec = new Button("Prec");
        Button valid = new Button("Valider");
        Button nouveau = new Button("Nouveau");
        Button suivant = new Button("Suiv");
        Button dernier = new Button("Dern");

        double buttonWidth = 100; // cela me permet de modifier la taille des boutons
        debut.setPrefWidth(buttonWidth);
        prec.setPrefWidth(buttonWidth);
        valid.setPrefWidth(buttonWidth);
        nouveau.setPrefWidth(buttonWidth);
        suivant.setPrefWidth(buttonWidth);
        dernier.setPrefWidth(buttonWidth);


        box.setPadding(new Insets(5)); //Je met des espaces entre les boutons top right bottom left
        box.getChildren().addAll(debut, prec, valid, nouveau, suivant, dernier);
        box.setAlignment(Pos.CENTER);
        root.setLeft(box);
    }
    public void ajouteCentre(BorderPane root){
        GridPane pane = new GridPane();//ici je crée un GridPane pour mettre les éléments
        pane.setHgap(10);
        pane.setStyle("-fx-background-color: #20C997;"); //Je change la couleur du fond en vert
        Label pseudo = new Label("Pseudo : ");
        pseudo.setPadding(new Insets(10, 10, 10, 10));//top right bottom left
        TextField pseu = new TextField();
        Label motdepasse = new Label("Mot de passe : ");
        motdepasse.setPadding(new Insets(10, 10, 10, 10));//top right bottom left
        PasswordField mdp = new PasswordField();//Je cache le mot de passe avec PasswordField
        Label poste = new Label("Poste : ");
        poste.setPadding(new Insets(10, 10, 10, 10));//top right bottom left

        StackPane stack = new StackPane();
        ImageView eleve = new ImageView("images/harry.jpg");//J'ajoute yne image
        stack.getChildren().add(eleve);
        stack.setPadding(new Insets(10, 10, 10, 10));//top right bottom left

        ComboBox postes = new ComboBox();
        postes.getItems().addAll("Attaquant", "Arbitre", "Gardien", "Défenseur");


        //Ici je crée un TitledPane pour mettre les boutons radio
        TitledPane pane2 = new TitledPane("Niveau", pane);
        RadioButton niveau = new RadioButton("Débutant");
        RadioButton niveau2 = new RadioButton("Medium");
        RadioButton niveau3 = new RadioButton("Expert");
        TitledPane box = new TitledPane();
        VBox box2 = new VBox();
        box2.getChildren().addAll(niveau, niveau2, niveau3);
        box.setText("Niveau");
        box.setContent(box2);

        //Je fais maintenant les placements des éléments
        pane.add(pseudo, 0, 0); 
        pane.add(pseu, 1, 0);
        pane.add(motdepasse, 0, 1);
        pane.add(mdp, 1, 1);
        pane.add(poste, 0, 2);
        pane.add(postes, 1, 2);
        pane.add(stack, 3, 0, 1, 7);
        pane.add(box, 1, 3, 2, 1);


        root.setCenter(pane);
    }
    public void ajouteBas(BorderPane root){
        HBox box = new HBox();
        box.setStyle("-fx-background-color: #98FB98;"); //Je change la couleur du fond en rose
        Label gauche = new Label("Nb de fiches: 102");
        gauche.setPadding(new Insets(10, 10, 10, 10));
        Label droite = new Label("Fiche modifiée");
        Label central = new Label("Numero de la fiche: 10");
        box.getChildren().addAll(gauche, central, droite);
        central.setAlignment(Pos.CENTER);
        gauche.setAlignment(Pos.CENTER_LEFT);
        droite.setAlignment(Pos.CENTER_RIGHT);
        root.setBottom(box);
    }
    public static void main(String[] args) {
        launch(args);
    }
}