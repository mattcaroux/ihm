import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.*;

public class AppliConnexion extends GridPane{

    protected Label Titre;
    protected TextField Identifiant ;
    protected PasswordField MotDePasse ;
    private Button btnConnexion;


    public AppliConnexion(Button boutonCo) throws Exception {
        super();
        //BorderPane root = new BorderPane();
        this.btnConnexion = boutonCo;
        this.setVgap(10);
        this.setHgap(10);

        this.ajouteTitre();
        this.ajouteIdentifiant();
        this.ajouteMotDePasse();
        this.ajouteBoutons();
        this.setAlignment(Pos.CENTER);
        //root.setCenter(this); Je voulais mettre le gridpane au centre mais ça ne marche pas
    }


    public String getIdentifiant() {
        return this.Identifiant.getText();
    }
    public PasswordField getMotDePasse() {
        return MotDePasse;
    }


    private void ajouteBoutons() {
        this.add(btnConnexion, 1, 3);
    }


    private void ajouteMotDePasse() {
        Label labelMdp = new Label("Mot de passe : ");
        this.add(labelMdp, 0, 2);
        PasswordField motDePasse = new PasswordField();
        this.add(motDePasse, 1, 2);
    }


    private void ajouteIdentifiant() {
        Label labelId = new Label("Identifiant : ");
        this.add(labelId, 0, 1);
        TextField identifiant = new TextField("");
        this.add(identifiant, 1, 1);

    }


    private void ajouteTitre() {
        HBox hbTitre = new HBox(20);
        Label titre = new Label("Entrez votre identifiants et votre mot de passe");
        hbTitre.getChildren().addAll(titre);
        this.add(hbTitre, 0, 0, 3, 1);
    }
}