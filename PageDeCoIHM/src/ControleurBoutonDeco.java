import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.application.Platform;
import javafx.event.ActionEvent;

public class ControleurBoutonDeco implements EventHandler<ActionEvent>{

    private AppliContainer appli;

    public ControleurBoutonDeco(AppliContainer appli){
        this.appli = appli;
    }


    @Override
    public void handle(ActionEvent event){
    Button button = (Button) (event.getSource());
    if (button.getText().contains("Déconnexion")){
        try {
            this.appli.afficheFenetreConnexion();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Déconnexion");
    }
}

            
}
