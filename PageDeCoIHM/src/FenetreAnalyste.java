import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.*;

public class FenetreAnalyste extends BorderPane{

    protected Label Titre;
    protected TextField Identifiant ;
    protected TextField MotDePasse ;
    private Button btnDeconnexion;
    

    public FenetreAnalyste(Button boutonDeco) throws Exception {
        super();
        this.btnDeconnexion = boutonDeco;
        this.ajouteTitre();
        this.ajouteCentre();
        this.ajouteDroite();
    }

    public void ajouteTitre() {
        BorderPane bp2 = new BorderPane();
        Text text = new Text("Allo 45 - Mode Analyste");
        text.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        bp2.setLeft(text);
        ImageView user = new ImageView("images/user.png");
        bp2.setStyle("-fx-background-color: #336699;");
        bp2.setRight(this.btnDeconnexion);
        this.setTop(bp2);
        bp2.setPadding(new Insets(15, 0, 20, 0));//top, right, bottom, left
    }
    public void ajouteCentre() {
        VBox vBox = new VBox();
        VBox vBox2 = new VBox();//Juste créer pour pouvoir faire un padding
        Text text = new Text("Analyse du Sondage sur les habitudes alimentaires");
        vBox2.getChildren().add(text);
        vBox2.setPadding(new Insets(0, 0, 15, 0));
        text.setFont(Font.font("Arial", FontWeight.BLACK, 25));
        vBox.getChildren().add(vBox2);
        ComboBox comboBox = new ComboBox();
        comboBox.getItems().addAll("Sondage 1", "Sondage 2", "Sondage 3");
        comboBox.setValue("Choisir un sondage");
        vBox.getChildren().add(comboBox);
        PieChart chart = new PieChart () ;
        chart.setTitle("Que lisez-vous au petit déjeuner ?") ;
        chart.getData().setAll(
            new PieChart.Data("Le journal", 21) ,
            new PieChart.Data("Un livre", 3) ,
            new PieChart.Data(" Le courier", 7) ,
            new PieChart.Data("La boîte de céréales", 75) ) ;
        chart.setLegendSide(Side.LEFT) ; // pour mettre la légende à gauche
        vBox.setPadding(new Insets(15, 0, 20, 0));
        vBox.getChildren().add(chart);
        this.setCenter(vBox);
        this.ajouteHBox(vBox);
    }
    public void ajouteHBox(VBox vBox){
        HBox hBox = new HBox();
        ImageView qp = new ImageView("images/back.png");
        ImageView qs = new ImageView("images/next.png");
        Button button1 = new Button("Question précédente", qp);
        Button button2 = new Button("Question suivante", qs);
        hBox.getChildren().addAll(button1, button2);
        vBox.getChildren().add(hBox);


    }
    public void ajouteDroite() {
        TilePane tilePane = new TilePane();
        ImageView imageView0 = new ImageView("images/chart_1.png");
        tilePane.getChildren().add(imageView0);
        ImageView imageView1 = new ImageView("images/chart_2.png");
        tilePane.getChildren().add(imageView1);
        ImageView imageView2 = new ImageView("images/chart_3.png");
        tilePane.getChildren().add(imageView2);
        ImageView imageView3 = new ImageView("images/chart_4.png");
        tilePane.getChildren().add(imageView3);
        ImageView imageView4 = new ImageView("images/chart_5.png");
        tilePane.getChildren().add(imageView4);
        ImageView imageView5 = new ImageView("images/chart_6.png");
        tilePane.getChildren().add(imageView5);
        ImageView imageView6 = new ImageView("images/chart_7.png");
        tilePane.getChildren().add(imageView6);
        ImageView imageView7 = new ImageView("images/chart_8.png");
        tilePane.getChildren().add(imageView7);
        tilePane.setStyle("-fx-background-color: #87CEEB");
        this.setRight(tilePane);
    }
}