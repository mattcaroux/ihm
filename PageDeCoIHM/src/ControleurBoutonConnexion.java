import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;

public class ControleurBoutonConnexion implements EventHandler<ActionEvent>{

    private AppliContainer appli;

    public ControleurBoutonConnexion(AppliContainer appli){
        this.appli = appli;
    }


    @Override
    public void handle(ActionEvent event){
        Button button = (Button) (event.getSource());
        if (button.getText().contains("Connexion")){
                try {
                    this.appli.afficheFenetreAnalyste();
                } catch (Exception e) {
                    System.out.println("Erreur lors de l'affichage de la fenêtre Analyste");
                }
                System.out.println("Connexion");                
        }
    }
            
}