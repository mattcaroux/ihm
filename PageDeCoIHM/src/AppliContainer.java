import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.*;

public class AppliContainer extends Application{

    protected Button buttonConnection;
    protected Button buttonDeconnection;
    private Scene scene;


    @Override
    public void init(){
        this.buttonConnection = new Button("Connexion");
        this.buttonDeconnection = new Button("Déconnexion", new ImageView("images/user.png"));
        this.buttonConnection.setOnAction(new ControleurBoutonConnexion(this));
        this.buttonDeconnection.setOnAction(new ControleurBoutonDeco(this));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane root = new Connexion(this.buttonConnection);
        root.setPrefWidth(1050);
        root.setPrefHeight(550);
        //root.setVgap(10);
        //root.setHgap(10);

        //this.ajouteTitre(root);
        //this.ajouteIdentifiant(root);
        //this.ajouteMotDePasse(root);
        //this.ajouteBoutons(root);
        
        this.scene = new Scene(root);
        primaryStage.setTitle("Allo 45");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public void afficheFenetreAnalyste() throws Exception{
        FenetreAnalyste fenetreAnalyste = new FenetreAnalyste(this.buttonDeconnection);
        try {
            this.scene.setRoot(fenetreAnalyste);
        } catch (Exception e) {
            System.out.println("Erreur lors de l'affichage de la fenêtre Analyste");
        }
    }
    public void afficheFenetreConnexion() throws Exception{
        Connexion Connexion = new Connexion(this.buttonConnection);
        try {
            this.scene.setRoot(Connexion);
        } catch (Exception e) {
            System.out.println("Erreur lors de l'affichage de la fenêtre Connexion");
        }
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}